const prevButton = document.getElementById("prevButton");
const nextButton = document.getElementById("nextButton");
const carousel = document.querySelector(".carousel");

let currentIndex = 0;
let numVisibleItems = calculateVisibleItems();
let items = carousel.querySelectorAll(".product");
const totalItems = items.length;

function calculateVisibleItems() {
  const screenWidth = window.innerWidth;
  if (screenWidth >= 1200) {
    return 5;
  } else if (screenWidth >= 700) {
    return 3;
  } else {
    return 1;
  }
}

function showItems() {
  items = carousel.querySelectorAll(".product");
  numVisibleItems = calculateVisibleItems();

  items.forEach((item, index) => {
    if (index >= currentIndex && index < currentIndex + numVisibleItems) {
      item.style.display = "block";
    } else {
      item.style.display = "none";
    }
  });
}

function nextItem() {
  currentIndex = Math.min(currentIndex + 1, totalItems - numVisibleItems);
  showItems();
}

function prevItem() {
  currentIndex = Math.max(currentIndex - 1, 0);
  showItems();
}

prevButton.addEventListener("click", prevItem);
nextButton.addEventListener("click", nextItem);

showItems();

window.addEventListener("resize", function () {
  showItems();
});
